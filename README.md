# LUPDATER

This is initial Lupdater readme.

Its primary intention is to run, and update GNOME desktop launchers (some.desktop) files.
At this moment, you need to have multiple scripts with variables set correctly, to change
launchers.
   
However since the logic behind lupdater is quite strong, you can use it for whatever you 
need to address. It will run in loop and acts only on changes detected.

## Key concepts
        
* runs in INTERVAL loop, executing COMMAND, which output is considered to be STATE. 
* if STATE changes, launcher ~/Desktop/NAME.desktop icon is set to ICONS[STATE] 
* if STATE is unknown, ICONS[default] state is loaded
* if ICONS[default] key is not defined, icon is changed to harcoded one (grey cross)
* lupdater won't run multiple instances
* lupdater takes optional argument:
       * "exit" - terminates current lupdater instance
       * any mon-empty string, which will run "on_click" function in the script
         passing argument to it.
         lupdater then will continue, unless other instance is running already.

* it's common pattern to point launcher file to lupdater, optionally with argument.
  This will start lupdater, while "on_click" will toggle some operation.

